#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<string.h>
int main(void){

	int fd[2],nbytes;
	pid_t childpid;
	char string[] = "First test.c!\n";
	char readbuffer[80];

	pipe(fd);
	childpid = fork();
	if(childpid== -1){
	
		perror("fork");
		exit(1);	

	}

	if(childpid == 0){
		
	close(fd[0]);

	write(fd[1],string,(strlen(string)+1));
	
	}
	else {
	
	close(fd[1]);
	 read(fd[0],readbuffer,sizeof(readbuffer));
	printf("Recieved String: %s",readbuffer);
	
	}

	return 0;
}	
