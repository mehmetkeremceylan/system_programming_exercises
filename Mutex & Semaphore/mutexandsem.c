#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>

#define COUNT 10

pthread_mutex_t count_mutex;

sem_t count_semaphore;

int count;

void
*increment_count()
{
 //pthread_mutex_lock(&count_mutex);
 sem_wait(&count_semaphore);
 count = count + 1;
 printf("count is %d\n",count);
 sem_post(&count_semaphore);
// pthread_mutex_unlock(&count_mutex);
}

int main(void) {
//pthread_mutex_init(&count_mutex,NULL);
sem_init(&count_semaphore,0,1);
pthread_t th[COUNT];
for(int i=0;i<COUNT;i++)
pthread_create(&th[i],NULL,&increment_count,NULL);
for(int i=0;i<COUNT;i++)
pthread_join(th[i],NULL);
sem_destroy(&count_semaphore);
//pthread_mutex_destroy(&count_mutex);
return 0;
}