#include <pthread.h>
#include <stdio.h>
#define PI 3.14
#define N 10

void *T(void *r_type){
double perimeter = 0.0;
int r = (int) r_type;
perimeter = (double) (2 * PI * r);
pthread_exit((void *)&perimeter);
}

int main(void) {
pthread_t th[N+1];
double *threadArea_ptr; // returned value from thread
double totalArea; // total area that sum of all threads
totalArea = 0.0;
int i = 0;
for(i=1;i<N;i++)
pthread_create(&th[i],NULL,(void *) T,(void*)i);
for(i=1;i<N;i++) {
pthread_join(th[i],(void **)&threadArea_ptr);
totalArea+=*threadArea_ptr;
}

printf("Total area is %g",totalArea);
return 0;
}